@extends('layouts.app')
@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">DAFTAR HARGA AGEN</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="row">
                    <div class="col-md-2">
                      <a href="{{url('/updateagentpulsa')}}" class="btn-primary form-control">UPDATE PULSA</a>
                    </div>
                    <div class="col-md-2">
                      <a href="{{url('/updateagentpln')}}" class="btn-primary form-control">UPDATE PLN</a>
                    </div>
                    <div class="col-md-2">
                      <a href="{{url('/updateagentgame')}}" class="btn-primary form-control">UPDATE GAME</a>
                    </div>
                  </div>
                  <br>
                    <div class="table-responsive">

                    <table border="1" class="table table-responsive">
                      <tr>
                        <td width="5" align="center"><strong>No</strong></td>
                        <td align="center"><strong>Provider</strong></td>
                        <td align="center"><strong>Code</strong></td>
                        <td align="center"><strong>Description</strong></td>
                        <td align="center"><strong>Harga Agen</strong></td>
                        <td width="90" align="center"><strong>untung</strong></td>
                        <td align="center"><strong>Harga Server</strong></td>
                        <td align="center"><strong>Status</strong></td>
                        <td align="center"><strong>Update</strong></td>
                      </tr>
                      <?php $id=0;
                      $price=0;
                      $untung=0;?>
                      @foreach($products as $key)

                        <?php $id+=1;
                        $price+=$key->price;
                        $untung+=$key->untung;
                        ?>
                        <tr>
                          <td>{{$id}}</td>
                          <td align="center">{{ $key->created_at}}</td>
                          <td align="center">{{ $key->operator}}</td>
                          <td align="center">{{ $key->description}}</td>
                          <td align="right">{{ number_format($key->price)}}</td>
                          <td align="right">
                            <form method="post" action="{{route('admin.dathar')}}" id="{{$key->id}}">
                              {{ csrf_field() }}
                            <input type="hidden" name="id" value="{{$key->id}}">
                            <input type="number" class="form-control" name="price" value="{{ $key->untung }}"></td>
                          <td align="center">{{ number_format($key->price - $key->untung)}}</td>
                          <td align="center">{{ $key->status}}</td>
                          <td align="center">
                            <!-- <input type="submit" name="btn" value="PROSES"> -->
                            <button class="form-control btn-primary" onclick="event.preventDefault();
                                                     document.getElementById('{{$key->id}}').submit();">

                        		UPDATE</button>
                          </td>
                          </form>
                        </tr>

                      @endforeach
                      <tr>
                        <td colspan="4">TOTAL</td>
                        <td>{{number_format($price)}}</td>
                        <td>{{number_format($untung)}}</td>
                        <td colspan="2"></td>
                      </tr>


                    </table>

                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
