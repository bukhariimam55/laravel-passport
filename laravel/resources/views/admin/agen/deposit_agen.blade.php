@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">KONFIRMASI DEPOSIT</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    <div class="table-responsive">
                    <table border="1" class="table table-responsive">
                      <tr>
                        <td width="5" align="center"><strong>No</strong></td>
                        <td align="center"><strong>Tangal</strong></td>
                        <td align="center"><strong>ID User</strong></td>
                        <td align="center"><strong>Bank</strong></td>
                        <td align="center"><strong>Nominal Request</strong></td>
                        <td align="center"><strong>Nominal Transfer</strong></td>
                        <td align="center"><strong>Status</strong></td>
                        <td align="center"><strong>Action</strong></td>
                      </tr>
                      <?php $id=0; ?>
                      @foreach($detailsaldo as $key)
                      <?php $id+=1; ?>
                      <tr>
                        <td>{{$id}}</td>
                        <td align="center">{{ $key->created_at}}</td>
                        <td align="center">{{ $key->user_id}}</td>
                        <td align="center">{{ $key->bank}}</td>
                        <td align="right">{{ number_format($key->nominal)}}</td>
                        <td align="right">{{ number_format($key->transfer)}}</td>
                        <td align="center">{{ $key->status}}</td>
                        @if($key->status =='Menunggu')
                        <td align="center"><a onclick="confirm('Proses ?')" href="{{ url('/proses-dep-agen/'.$key->id) }}" class="btn-primary btn">Proses</a><a onclick="confirm('Batalkan ?')" href="{{ url('/cancel-dep-agen/'.$key->id) }}" class="btn-danger btn">Cancel</a></td>
                        @else
                        <td align="center"></td>
                        @endif
                      </tr>
                      @endforeach
                    </table>
                  </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
