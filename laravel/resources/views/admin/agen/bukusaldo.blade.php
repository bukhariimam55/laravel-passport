@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">BUKU SALDO AGEN</div>

                <div class="panel-body">
                  <form class="" action="{{route('bukusaldo')}}" method="post">
                    {{ csrf_field() }}
                    <table>
                      <tr>
                        <td>ID AGEN</td>
                        <td>: <input type="number" name="id_agen" value="" class=""></td>
                      </tr>
                      <tr>
                        <td>BULAN</td>
                        <td>: <select class="" name="bulan">
                         <?php $bulan =13;
                         for ($i=1; $i < $bulan ; $i++) {
                           echo "<option value='".$i."'>".$i."</option>";
                         }
                         ?>
                       </select></td>
                      </tr>
                      <tr>
                        <td>TAHUN</td>
                        <td>: <select class="" name="tahun">
                         <option value="2018">2018</option>
                         <option value="2019">2019</option>
                       </select></td>
                      </tr>
                    </table>
                    <input type="submit" name="btn" value="CARI">
                  </form>
                  @if(count($datas)>0)
                    <br><br><br>
                    <h5><strong>DETAIL</strong></h5>
                    <table>
                      <tr>
                        <td>ID </td>
                        <td> : {{$users->id}}</td>
                      </tr>
                      <tr>
                        <td>PERUSAHAAN </td>
                        <td> : {{$users->name}}</td>
                      </tr>
                      <tr>
                        <td>NO TELP </td>
                        <td> : {{$users->hp}}</td>
                      </tr>
                      <tr>
                        <td>SALDO AKHIR </td>
                        <td> : Rp {{number_format($users->saldo)}}</td>
                      </tr>
                    </table>

                  <br><br><br>
                  <div class="table-responsive">
                    <table class="table">
                      <tr>
                        <th>Tgl Trx</th>
                        <th>No Trx</th>
                        <th>Kredit</th>
                        <th>Debet</th>
                        <th>Saldo</th>
                        <th>keterangan</th>
                      </tr>
                      <?php $no=1;?>
                      @foreach($datas as $key=> $data)
                      <tr>
                        <td>{{date('d-m-Y H:i',strtotime($data->created_at))}}</td>
                        <td>{{$data->no_trx}}</td>
                        @if($data->mutasi =='Debet')
                        <td align="right">-</td>
                        <td align="right">Rp {{number_format($data->nominal)}}</td>
                        @else
                        <td align="right">Rp {{number_format($data->nominal)}}</td>
                        <td align="right">-</td>
                        @endif
                        <td>Rp {{number_format($data->saldo)}}</td>
                        <td>{{$data->keterangan}}</td>
                      </tr>
                      @endforeach
                    </table>
                  </div>
                  @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
