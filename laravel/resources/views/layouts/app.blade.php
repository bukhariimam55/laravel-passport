<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>FIXPAY</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
</head>
<body>

  <?php

$show = App\Apis::where('active',1)->first();
$url = $show->url_server;
$header = array(
'h2h-userid:'.$show->userid,
'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
'h2h-secret:'.$show->secret, // lihat hasil autogenerate di member area
);

$data = array(
'inquiry' => 'S', // konstan
);

$ch = curl_init();
curl_setopt($ch, CURLOPT_URL, $url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
curl_setopt($ch, CURLOPT_POST, 1);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
$result = curl_exec($ch);

 $json = json_decode($result, true);?>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        FIXPAY
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                        &nbsp;
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest

                        @else
                          @if(Auth::user()->tipe =="user")
                          @else

                            <li><a href="{{ url('/home') }}">HOME</a></li>
                            <li><a href="{{ url('/users') }}">DATA USERS</a></li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">DEPOSIT <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="{{ url('/condef') }}">Konfirmasi Deposit</a></li>
                                  <li><a href="{{ url('/konfirmasi-deposit-agen') }}">Konfirmasi Deposit Agen</a></li>
                                  <li><a href="{{ url('/datadeposit') }}">Data Deposit</a></li>
                                  <li><a href="{{ route('bukusaldo') }}">Buku Saldo Agen</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">DATA TRANSAKSI <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="{{ url('/datpul') }}">Transaksi NonAgen</a></li>
                                  <li><a href="{{ url('/transaksi-agent') }}">Transaksi Agen</a></li>
                                </ul>
                            </li>
                            <li class="dropdown">
                              <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">DATA HARGA <span class="caret"></span></a>
                                <ul class="dropdown-menu">
                                  <li><a href="{{ url('/dathar') }}">Data Harga USer</a></li>
                                  <li><a href="{{ url('/agent') }}">Data Harga Agen</a></li>
                                </ul>
                            </li>
                          @endif
                          @include('flash::message')
                            <li class="dropdown">

                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="#">Saldo : {{ number_format($json['balance'])}}</a></li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            LOGOUT
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <!-- <script src="//code.jquery.com/jquery.js"></script> -->
    <!-- <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script> -->
</body>
</html>
