@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12 col-md-offset-0">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if(Auth::user()->tipe =='user')
                    Mohon maaf, anda tidak boleh akses halaman ini.
                    @else
                    Selamat Datang Admin : {{Auth::user()->name}}
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
