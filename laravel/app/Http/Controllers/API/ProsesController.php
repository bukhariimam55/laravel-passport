<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Prov_token;
use Log;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Apis;
use App\Detailsaldos;
use App\Hrgvoucherlistrik;
use App\Transactions;
use App\User;
use App\Products;

class ProsesController extends Controller
{
  public function prosespulsa(Request $request){
    $iduser=$request->user()->id;
    Log::info($iduser);
    Log::info($request);
    date_default_timezone_set('Asia/Jakarta');

    $show = Apis::where('active',1)->first();
    $subjects = $request->paket;
    $nomor = $request->nomorhp;
    $startDate = date("Y-m-d");
		$endDate = date("Y-m-d");
    if (Transactions::whereBetWeen('created_at',[$startDate . ' 00:00:00', $endDate . ' 23:59:59'])->where('paket',$subjects)->where('hp_id_pel',$nomor)->where('status','Sukses')->first()) {
      $response = [
        'error'=>true,
        'message'=>'Transaksi ini sudah ada yang sukses'
      ];
      return response()->json($response);
    }elseif (Transactions::whereBetWeen('created_at',[$startDate . ' 00:00:00', $endDate . ' 23:59:59'])->where('paket',$subjects)->where('hp_id_pel',$nomor)->where('status','Pending')->first()) {
      $response = [
        'error'=>true,
        'message'=>'Transaksi ini sudah ada dalam proses'
      ];
      return response()->json($response);
    }

    $detail = Products::where('description',$subjects)->first();
    $paxpaid = $detail->price + $detail->untung;

    if ($request->user()->saldo < $paxpaid) {
      $response = [
        'error'=>true,
        'message'=>'Saldo anda kurang'
      ];
      return response()->json($response);
    }
    $transaksiid = date('YmdHis').$iduser;
    $url = $show->url_server;

    $header = array(
    'h2h-userid:'.$show->userid,
    'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
    'h2h-secret:'.$show->secret, // lihat hasil autogenerate di member area
    );

    if($detail->provider_sub == 'PLN'){
      $data = array(
      'inquiry' => 'PLN', // konstan
      'code' => $detail->code, // kode produk
      'idcust' => $nomor, // nohp meteran
      'trxid_api' => $transaksiid, // Trxid / Reffid dari sisi client
      );
    }else{
      $data = array(
      'inquiry' => 'I', // konstan
      'code' => $detail->code, // kode produk
      'phone' => $nomor, // nohp pembeli
      'trxid_api' => $transaksiid, // Trxid / Reffid dari sisi client
      );
    }


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $result = curl_exec($ch);
    $json = json_decode($result, true);
    $aler = $json['result'];
    // $aler = 'success';
    // $message = $json['message'];
    if ($aler=='success') {
      Log::info('SUKSES : ALERt');
      $debet = User::find($iduser);
      $debet->saldo = $debet->saldo - $paxpaid;

      $transaction = Transactions::create([
        'userid'=>$iduser,
        'transactionid'=>$transaksiid,
        'hp_id_pel'=>$nomor,
        'paket'=>$subjects,
        'harga'=>$paxpaid,
        'nta'=>$detail->price,
        'status'=>'Pending',
        'sn'=>null,
        'created_at'=>date('Y-m-d H:i:s'),
        'saldo'=>$debet->saldo
      ]);
      $debet->update();
      $response = [
        'error'=>false,
        'message'=>$subjects.' Segera Diproses'
      ];
    }else {
      Log::info('GAGAL : ALERt');
      $response = [
        'error'=>true,
        'message'=>$subjects.' Gagal'
      ];
    }
    // $akhir = $hasil;
    return response()->json($response);

  }

  public function tambahsaldo(Request $request){
    $tambah = Detailsaldos::create([
      'iduser' => $request->user()->id,
      'bank'=> $request->bank,
      'nominal' => $request->nominal,
      'status' => 'Pending',
    ]);
    if ($tambah) {

    $url = "http://www.freesms4us.com/kirimsms.php?";
    $text = 'Deposit ID : '.$request->user()->id.', Bank '.$request->bank.', Rp. :'.number_format($request->nominal);

    $postfields = array(
        'user' => "iyhmam123",
        'pass' => "iyhmamsmart",
        'no' => "082312543008",
        'isi' => $text,
        'ket' => "yes"
    );
    if (!$curld = curl_init()) {
        exit;
    }
    curl_setopt($curld, CURLOPT_POST, true);
    curl_setopt($curld, CURLOPT_POSTFIELDS, $postfields);
    curl_setopt($curld, CURLOPT_URL,$url);
    curl_setopt($curld, CURLOPT_RETURNTRANSFER, true);
    $output = curl_exec($curld);
    curl_close ($curld);
    Log::info('SMS : '.$output.' dan '.$curld);

      $response = [
        'error'=>false,
        'message'=>'berhasil'
      ];
      echo json_encode($response);
      // return response()->json($response, 201);
    }else{
      $response[] = [
        'error'=>true,
        'message'=>'Gagal'
      ];
      echo json_encode($response);
    }

  }

  public function updateproduct(Request $request){

    $request->user()->id;
    $show = Apis::where('active',1)->first();

    $url = $show->url_server;

    $header = array(
    'h2h-userid:'. $show->userid,
    'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
    'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
    );

  // echo json_encode($header);
    $data = array(
    'inquiry' => 'HARGA', // konstan
    'code' => 'game', // pilihan: pln, pulsa, game
    );


    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
    curl_setopt($ch, CURLOPT_POST, 1);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
    $result = curl_exec($ch);
    $json = json_decode($result, true);
    $hasil = $json['message'];
    // return json_encode($hasil,201);
    foreach ($hasil as $value) {
      if ($produks = Products::where('code',$value['code'])->first()) {
            $produks->price = $value['price'];
            $produks->status = $value['status'];
            $produks->update();
      }
      // $produks = Products::create([
      //   'code'=>$value['code'],
      //   'description'=>$value['description'],
      //   'operator'=>$value['operator'],
      //   'price'=>$value['price'],
      //   'status'=>$value['status'],
      //   'provider_sub'=>$value['provider_sub']
      // ]);
    }
    // return response()->json($response,201);
  }

  public function editpass(Request $request){
    Log::info(' ALERt :'.$request);
    if(\Hash::check($request->input('passlm'), $request->user()->password))
    {
        $user = User::find($request->user()->id);
        $user->password = bcrypt($request->input('passbr'));
        $user->update();
        $response = [
          'error'=>false,
          'message'=>'Berhasil di update'
        ];
        echo json_encode($response);
      }else{
        $response = [
          'error'=>true,
          'message'=>'Password Lama SALAH'
        ];
        echo json_encode($response);
      }
  }
}
