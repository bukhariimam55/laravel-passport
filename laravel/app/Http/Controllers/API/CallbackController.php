<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Log;

class CallbackController extends Controller
{
  public function agent(Request $request){
    Log::info($_SERVER['REMOTE_ADDR']);
    Log::info('RESPON JAVAH2H CALLBACK NON IP:'.$_POST['content']);
  }
}
