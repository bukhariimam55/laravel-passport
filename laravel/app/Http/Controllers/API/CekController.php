<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Prov_token;
use Log;
use Illuminate\Support\Facades\Auth;
use Validator;
use App\Apis;
use App\Detailsaldos;
use App\Hrgvoucherlistrik;
use App\Transactions;
use App\Products;
use App\Agen;
use App\Bukusaldoagen;
use App\Datadepositagen;

class CekController extends Controller
{
//DATA TRANSAKSI SALDO
    public function datasaldo($id){
      $from = date('Y-m-01');
		  $until = date('Y-m-t');
      // echo json_encode($from.".".$until);
      $detail = Detailsaldos::where("iduser",$id)->whereBetween('created_at', [$from . ' 00:00:00', $until . ' 23:59:59'])->orderBy('id','DESC')->get();
        echo json_encode($detail,201);
      // return response()->json($detail,201);
    }
    public function cekharga(Request $request){
      $product = Products::where('description', $request->paket)->first();
      return response()->json($product);
    }
    //HARGA VOUCHER LISTRIK
    public function hargavoucherlistrik(Request $request){
      $show = Apis::where('active',1)->first();

      $url = $show->url_server;

      $header = array(
      'h2h-userid:'. $show->userid,
      'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
      'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
      );

    // echo json_encode($header);
      $data = array(
      'inquiry' => 'HARGA', // konstan
      'code' => 'PLN', // pilihan: pln, pulsa, game
      );


      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      $result = curl_exec($ch);
      $json = json_decode($result, true);
      $hasil = $json['message'];
      return $hasil;
    }

    public function listvoucherlistrik(){
      $list = Hrgvoucherlistrik::where('provider','Voucher-Listrik')->get();
        return response()->json($list,201);
    }

    public function hargapulsa($id){
      $product = Products::where('provider_sub',$id)->orderBy('id','ASC')->get();
      Log::info('Cek harga pulsa:'.$product);
      // return response()->json($product,201);
        echo json_encode($product,201);
      }

    public function hargaagent($id){
      if ($_SERVER['REMOTE_ADDR'] =="103.247.9.57") {
        $product = Agen::orderBy('id','ASC')->get();
        // Log::info('Cek harga pulsa:'.$product);
        // return response()->json($product,201);
          echo json_encode($product,201);
      }
      Log::info('IP KOPERASI:'.$_SERVER['REMOTE_ADDR']);
    }

    public function datatransaksi($id){
      $data = Transactions::where('userid',$id)->orderBy('id','DESC')->get();
      return response()->json($data,201);
    }

    public function cekStatus(Request $request){
      $cek = Transactions::find($request->id);
      if ($cek->transactionid =='12345') {
        $response = [
          'status'=>'Gagal atau Refund'
          ];
          echo json_encode($response);
      }
      $show = Apis::where('active',1)->first();
      $url = $show->url_server;

      $header = array(
      'h2h-userid:'. $show->userid,
      'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
      'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
      );

      $data = array(
      'inquiry' => 'STATUS', // konstan
      'trxid_api' => $cek->transactionid, // Trxid atau Reffid dari sisi client saat transaksi pengisian
      );

      $ch = curl_init();
      curl_setopt($ch, CURLOPT_URL, $url);
      curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
      curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
      curl_setopt($ch, CURLOPT_POST, 1);
      curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
      $result = curl_exec($ch);
      $json = json_decode($result, true);
      Log::info("Cek result:".$result);
      // echo json_encode($result);
      $message = $json['message'][0]['status'];
      if ($cek->status =='Sukses') {
        if ($message == 4) {
          $update = Transactions::where('transactionid',$json['message'][0]['trxid_api'])->first();
          $update->status = 'Sukses';
          $update->sn = $json['message'][0]['sn'];
          $update->update();
          $response = [
            'status'=>'Sukses'
          ];
        }
      }elseif ($cek->status == 'Pending') {
        if ($message == 1) {
          $response = [
            'status'=>'Pending'
          ];
        }elseif ($message == 2) {
          $response = [
            'status'=>'Gagal'
          ];
        }elseif ($message == 3) {
          $response = [
            'status'=>'Refund'
          ];
        }else{
          $update = Transactions::where('transactionid',$json['message'][0]['trxid_api'])->first();
          $update->status = 'Sukses';
          $update->sn = $json['message'][0]['sn'];
          $update->update();
          $response = [
            'status'=>'Sukses'
          ];
        }
      }else {
          $response = [
            'status'=>'Gagal atau Refund'
            ];
      }
      echo json_encode($response);

      // Log::info("Cek result:" . $result-result);
      // return response()->json($message,201);
    }
    public function provider(Request $request){
      // Log::info("request cilegon:".$request->all());
      $products = Products::where('provider_sub','REGULER')->get();
      return response()->json($products,201);
    }

}
