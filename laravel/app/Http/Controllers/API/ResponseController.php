<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Prov_token;
use Log;
use Illuminate\Support\Facades\Auth;
use App\Transaksiagen;
use Validator;
use App\Apis;
use App\Details;
use App\Hrgvoucherlistrik;
use App\Transactions;
use App\User;
use App\Products;
use App\Bukusaldoagen;
use DB;

class ResponseController extends Controller
{
    public function callback(Request $request)
    {

      // return response()->json($request->all());

        Log::info($_SERVER['REMOTE_ADDR']);
        Log::info('RESPON JAVAH2H CALLBACK NON IP:'.$request->content);
        if ($_SERVER['REMOTE_ADDR'] == '172.104.161.223') { // memastikan data terikirim dari server portalpulsa
            Log::info('RESPON JAVAH2H CALLBACK:'.$request->content);
            $kode=json_decode($request->content, true);
            if (Transaksiagen::where('trxid_api', $kode['trxid_api'])->first()) {
                if ($kode['status']==4) {
                    $sn = $kode['sn'];
                    $datas = Transaksiagen::where('trxid_api', $kode['trxid_api'])->first();
                    $datas->status = 'Berhasil';
                    $datas->sn = $kode['sn'];
                    $trxid_api = $kode['trxid_api'];
                    $status = $kode['status'];
                    $note ='Transaksi '.$datas->paket.' Berhasil ('.$kode['trxid_api'].')';
                    $data = array("trxid_api" => $trxid_api, "status" => $status, "note" => $note, "ip"=>"103.247.10.253", "sn"=>$sn);
                    $urlcallback = User::find($datas->user_id);
                    $ch = curl_init($urlcallback->callback);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $datas->update();
                } else {
                    $datas = Transaksiagen::where('trxid_api', $kode['trxid_api'])->first();
                    $datas->status ='Gagal';
                    $status = $kode['status'];
                    $saldo = User::find($datas->user_id);
                    $total = $saldo->saldo + $datas->harga;
                    $saldo->saldo = $total;
                    $note ='Gagal No transaksi ('.$kode['trxid_api'].')';
                    Bukusaldoagen::create([
                      'user_id'=>$datas->user_id,
                      'no_trx'=>date('Ymdhi').$datas->user_id,
                      'tgl_trx'=>date('Y-m-d'),
                      'nominal'=>$datas->harga,
                      'mutasi'=>'Debet',
                      'saldo'=>$total,
                      'keterangan'=>$note,
                      'aktif'=>1
                    ]);
                    $saldo->update();
                    $trxid_api = $kode['trxid_api'];
                    $data = array("trxid_api" => $trxid_api, "status" => $status, "note" => $note, "ip"=>"103.247.10.253");
                    $urlcallback = User::find($datas->user_id);
                    $ch = curl_init($urlcallback->callback);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
                    $response = curl_exec($ch);
                    curl_close($ch);
                    $datas->update();
                }
            } else {
                // code...
                if ($kode['status']==4) {
                    $update_nta = Products::where('code', $kode['code'])->first();
                    $update_nta->price = $kode['price'];
                    $transaksi = Transactions::where('transactionid', $kode['trxid_api'])->first();
                    $transaksi->status = 'Sukses';
                    $transaksi->sn = $kode['sn'];
                    $transaksi->note = $kode['note'];
                    $transaksi->update();
                    $update_nta->update();
                } elseif ($kode['status']==2) {
                    $transaksi = Transactions::where('transactionid', $kode['trxid_api'])->first();
                    $transaksi->status = 'Gagal';
                    $transaksi->sn = 'Mohon di cek kembali';
                    $kembali = User::find($transaksi->userid);
                    $kembali->saldo = $kembali->saldo + $transaksi->harga;
                    $transaksi->saldo = $kembali->saldo;
                    $transaksi->note = $kode['note'];
                    $kembali->update();
                    $transaksi->update();
                } else {
                    $transaksi = Transactions::where('transactionid', $kode['trxid_api'])->first();
                    if ($transaksi->status == 'Sukses') {
                        $transaksi->status = 'Refund';
                        $transaksi->sn = 'Mohon di cek kembali';
                        $kembali = User::find($transaksi->userid);
                        $kembali->saldo = $kembali->saldo + $transaksi->harga;
                        $transaksi->saldo = $kembali->saldo;
                        $kembali->update();
                        $transaksi->update();
                    }
                }
                //end
            }
        }
    }
    public function testback(Request $request)
    {
        $datas = Transaksiagen::where('trxid_api', $request['trxid_api'])->first();
        $datas->status = 'Gagal';
        $status = $request['status'];
        $saldo = User::find($datas->user_id);
        $total = $saldo->saldo + $datas->harga;
        $saldo->saldo = $total;
        $note ='Gagal No transaksi ('.$request['trxid_api'].')';
        Bukusaldoagen::create([
        'user_id'=>$datas->user_id,
        'no_trx'=>date('Ymdhi').$datas->user_id,
        'tgl_trx'=>date('Y-m-d'),
        'nominal'=>$datas->harga,
        'mutasi'=>'Debet',
        'saldo'=>$total,
        'keterangan'=>$note,
        'aktif'=>1
      ]);
        $saldo->update();
        $datas->update();
        $data = array("trxid_api" => $request['trxid_api'], "status" => $request['status'], "note" => $request['note'], "ip"=>"103.247.10.253");
        $urlcallback = User::find(41);
        $ch = curl_init($urlcallback->callback);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }
}
