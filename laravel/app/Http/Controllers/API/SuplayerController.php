<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Agen;
use App\Apis;
use App\Datadepositagen;
use App\Bukusaldoagen;
use App\Transaksiagen;
use App\User;
use DB;
use Log;

class SuplayerController extends Controller
{

    public function prosesagent(Request $request)
    {
        $harga = Agen::where('code', $request['code'])->first();
        if ($request->user()->saldo < $harga->price) {
            $respon = [
              'result'=>'error',
              'message'=>'Saldo Koperasi tidak mencukupi'
            ];
            return response()->json($respon);
        }
        if ($harga->status =='gangguan') {
            $respon = [
              'result'=>'error',
              'message'=>'Produk Tersebut sedang gangguan'
            ];
            return response()->json($respon);
        }

        DB::beginTransaction();
        $saldosss = $request->user()->saldo - $harga->price;
        try {
            $berhasil = Transaksiagen::create([
            'user_id'=>$request->user()->id,
            'trxid_api'=>$request['trxid'],
            'hp_id_pel'=>$request['nomor'],
            'inquiry'=>$harga->provider_sub,
            'paket'=>$harga->description,
            'harga'=>$harga->price,
            'nta'=>$harga->price - $harga->untung,
            'status'=>'Proses',
            'sn'=>null,
            'saldo'=>$saldosss,
            'note'=>'Transaksi '.$harga->description,
          ]);
            $saldos = User::find($request->user()->id)->update([
              'saldo'=>$saldosss,
            ]);
            $buku = Bukusaldoagen::create([
              'user_id'=>$request->user()->id,
              'no_trx'=>$request['trxid'],
              'tgl_trx'=>date('Y-m-d'),
              'nominal'=>$harga->price,
              'mutasi'=>'Kredit',
              'saldo'=>$saldosss,
              'aktif'=>1,
              'keterangan'=>'Transaksi '.$harga->description.' '.$request['nomor'],
              'created_by'=>$request->user()->id
            ]);
        } catch (\Exception $e) {
            Log::info('Gagal Edit Profil:'.$e->getMessage());
            DB::rollback();
            return Response()->json([
              'result'=>'error',
              'message'=>'transaksi gagal'
            ]);
        }
        DB::commit();
        Log::info("Reques Token:".$request->token);
        //TES RESPONSE
        // $respon = [
        //   'result'=>'success',
        //   'message'=>'Tes respon Akan diproses'
        // ];
        // return response()->json($respon);
        // ??/
        if ($request->user()->ijin_transaksi == 0) {
          $data['result'] = "success";
          $data['message'] = "Demo ".$harga->description." Akan diproses";
          $json = json_encode($data);
          $respon = json_decode($json, true);
          return response()->json($respon);
        }else {
          $show = Apis::where('active',1)->first();
          $url = $show->url_server;
          $header = array(
            'h2h-userid:'. $show->userid,
            'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
            'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
            );
          $code = $request['code']; // kode produk
          $nomorhp = $request['nomor']; // nohp pembeli
          $trxid_api = $request['trxid']; // Trxid / Reffid dari sisi client
          if ($harga->provider_sub == "PLN") {
            $data = array(
              'inquiry' => 'PLN', // konstan
              'code' => $code, // kode produk
              'idcust' => $nomorhp, // nohp pembeli
              'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
            );
          }else {
            $data = array(
              'inquiry' => 'I', // konstan
              'code' => $code, // kode produk
              'phone' => $nomorhp, // nohp pembeli
              'trxid_api' => $trxid_api, // Trxid / Reffid dari sisi client
            );
          }


          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
          $result = curl_exec($ch);
          $respon = json_decode($result, true);
          Log::info("RESPON TRANSAKSI PULSA :".$result);
          $kembali = $saldosss + $harga->price;
          if ($respon['result']=='success') {
              $data['result'] = "success";
              $data['message'] = "Transaksi ".$harga->description." Akan diproses";
              $json = json_encode($data);
              $respon = json_decode($json, true);
              return response()->json($respon);
          } else {
              $saldos = User::find($request->user()->id)->update([
                'saldo'=> $kembali,
              ]);
              Bukusaldoagen::where('no_trx', $request['trxid'])->first()->delete();
              Transaksiagen::where('trxid_api', $request['trxid'])->first()->delete();
              return response()->json($respon);
          }
        }

    }

    public function suplayer(Request $request)
    {
        $show = Apis::where('active',1)->first();
        $url = $show->url_server;

        $header = array(
          'h2h-userid:'. $show->userid,
          'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
          'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
          );

              $data = array(
          'inquiry' => 'D', // konstan
          'bank' => $request->bank, // bank tersedia: bca, bni, mandiri, bri, muamalat
          'nominal' => $request->nominal, // jumlah request
          );
          if ($request->user()->ijin_transaksi == 0) {
            $rand = rand(900,999);
            $nominal = (int)$request->nominal+$rand;
            $respon = [
              'result'=>'success',
              'message'=>'Silakan transfer sebesar Rp '.number_format($nominal,0,',','.').',- Ke Rekening: '.$request->bank.', no. 123456789, a.n. IMAM BUKHARI. Batas waktu transfer 1x24jam <br> <h3> INI HANYA CONTOH DEMO SYSTEM </h3>'
            ];
            return response()->json($respon);
          }
        if ($request->bank !== 'BNI') {
          $ch = curl_init();
          curl_setopt($ch, CURLOPT_URL, $url);
          curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
          curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
          curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
          curl_setopt($ch, CURLOPT_POST, 1);
          curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
          $result = curl_exec($ch);
          Log::info("RESPON TOPUP JAVA :".$result);
          $respon = json_decode($result, true);
        }else {
          if ($request->nominal < 500000) {
            $respon = [
              'result'=>'error',
              'message'=>'Minimal topup Rp 500.000,-'
            ];
            return response()->json($respon);
          }
          $rand = rand(900,999);
          $nominal = (int)$request->nominal+$rand;
          $data['result']="success";
          $data['message'] ="Silakan transfer sebesar Rp ".number_format($nominal,0,',','.').",- Ke Rekening: BNI, no. 0415838248, a.n. Imam Bukhari. Batas waktu transfer 1x24jam";
          $result = json_encode($data);
          $respon = json_decode($result, true);
        }

        // $result =  {
        //   "result": "success",
        //   "message": "Silakan transfer sebesar Rp 100.972,- Ke Rekening: BCA, no. 0770520207, a.n. BENY ARIF L. Batas waktu transfer 1x24jam"
        // }
        // $respon = [
        //   'result'=>'success',
        //   'message'=> "Silakan transfer sebesar Rp 500.972,- Ke Rekening: BCA, no. 0770520207, a.n. BENY ARIF L. Batas waktu transfer 1x24jam"
        // ];

        if ($respon['result']=='success') {
            $message = $respon['message'];
            $pesan = preg_replace('/[^A-Za-z0-9\ ]/', '', $message);
            Log::info("PESAN :".$pesan);
            $transfer = explode(" ", $pesan);
            Log::info("PESAN 5:".$transfer[4]);
            $nominal_transfer = $transfer[4];
            $datas = Datadepositagen::create([
              'user_id'=>$request->user()->id,
              'no_trx'=>$request->trxid,
              'tgl_trx'=>date('Y-m-d'),
              'bank'=>$request->bank,
              'transfer'=>$nominal_transfer,
              'nominal'=>$request->nominal,
              'status'=>'Menunggu',
              'ket'=>$message,
              'aktif'=>1,
              'respon_json'=>$result,
              'created_by'=>$request->user()->id
            ]);
        }
        return response()->json($respon);
    }
    public function datasuplayer(Request $request)
    {
        // $dari = date('Y-m-01');
        // $sampai = date('Y-m-d');
        if ($request->until && $request->from) {
            $dari = $request->from;
            $sampai = $request->until;
        }

        $status = $request->status;
        $datas = Datadepositagen::whereBetween('tgl_trx', [$dari,$sampai])->where('user_id', $request->user()->id)->where('status', 'LIKE', '%'.$status.'%')->where('aktif', 1)->get();
        return response()->json($datas);
    }
    public function bukusaldosuplayer(Request $request)
    {
        $dari = date('Y-m-01');
        $sampai = date('Y-m-d');
        if ($request->until && $request->from) {
            $dari = $request->from;
            $sampai = $request->until;
        }
        $users = User::find($request->user()->id);
        $datas = Bukusaldoagen::whereBetween('tgl_trx', [$dari,$sampai])->where('user_id', $request->user()->id)->where('aktif', 1)->orderBy('id', 'ASC')->get();
        $data = [
      'users'=>$users,
      'bukusaldo'=>$datas
    ];
        return response()->json($data);
    }
    public function cek_status_agent(Request $request)
    {
        $cekstatus = Transaksiagen::where('trxid_api', $request->no_trx)->first();
        if ($request->status !== $cekstatus->status) {
            $message = [
        'status'=>3,
        'message'=>'Status Beda'
      ];
            return response()->json();
        } else {
            $message = [
        'status'=>2,
        'message'=>'Status Sama'
      ];
            return response()->json($message);
        }
        $show = Apis::where('active',1)->first();
        $url = $show->url_server;

        $header = array(
    'h2h-userid:'. $show->userid,
    'h2h-key: '.$show->key, // lihat hasil autogenerate di member area
    'h2h-secret:'. $show->secret, // lihat hasil autogenerate di member area
    );

        $data = array(
    'inquiry' => 'STATUS', // konstan
    'trxid_api' => $request->no_trx, // Trxid atau Reffid dari sisi client saat transaksi pengisian
    );

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        $result = curl_exec($ch);
        $respon = json_decode($result, true);
        log::info('RESPON STATUS: '.$result);
        if ($respon['result']=='success') {
            if ($respon['message'][0]['status'] == 4) {
                Transaksiagen::where('trxid_api', $respon['message'][0]['trxid_api'])->first()->update([
          'status'=>'Berhasil'
        ]);
            }
            $message = $respon['message'][0];
        } else {
            $message = [
        'result'=>'failed',
        'message'=>'Data tidak ditemukan.'
      ];
        }
        return response()->json($message);
    }
    public function hargaagent($id){
        $product = Agen::orderBy('id','ASC')->get();
        // Log::info('Cek harga pulsa:'.$product);
        // return response()->json($product,201);
          echo json_encode($product,201);
      Log::info('IP KOPERASI:'.$_SERVER['REMOTE_ADDR']);
    }
}
