<?php

namespace App\Http\Controllers\API;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use App\Paketcuci;
use Illuminate\Support\Facades\Auth;
use Validator;
use Log;

class PassportController extends Controller
{
    public $successStatus = 200;

    public function login(Request $request){
      if (Auth::attempt(['email'=>$request->email, 'password'=> $request->password])) {
        $user = Auth::user();
        $success = $user->createToken('MyApp')->accessToken;
        $data = [
          'id'=>$user->id,
          'name'=>$user->name,
          'email'=>$user->email,
          'hp'=>$user->hp,
          'gender'=>$user->gender,
          'token'=>$success
        ];
        Log::info("TOKEN:" . $success);
        return Response()->json([
          'error' => false,
          'message' => 'Login Berhasil',
          'user' => $data
        ], $this->successStatus);
      }
      else{
        $response = [
          'error'=>true,
          'message'=>'Email atau Password Salah'
        ];
        return response()->json($response);
      }
    }

    public function register(Request $request){
      Log::info("All request Register:" . $request);
      $validator = Validator::make($request->all(),[
        'name' => 'required',
        'email' => 'required|email|unique:users,email',
        'password' => 'required|min:6',
        'hp' => 'required|min:10',
        'gender' => 'required'
      ],[
        'email.unique' => 'Email sudah di gunakan',
        'password.min'=>'Password Minimal 6',
        'hp.min'=>'Nomor HP minimal 10'
      ]);
      if ($validator->fails()) {
        $eror = json_decode($validator->errors());

        $response =[
            'error' => true,
            'message' => $eror
        ];
        return Response()->json($response);
      }

      $user = User::create([
        'name' => $request->name,
        'email'=> $request->email,
        'hp' => $request->hp,
        'gender' => $request->gender,
        'saldo'=>0,
        'password' => bcrypt($request->password)
      ]);
      // $success = $user->createToken('MyApp')->accessToken;
      $data = [
        'id'=>1,
        'name'=>$request->name,
        'email'=>$request->email,
        'hp'=>$request->hp,
        'gender'=>$request->gender,
        'token'=>123456
      ];
      return Response()->json([
        'error' => false,
        'message' => 'User registered successfully',
        'user'=>$data

      ], $this->successStatus);
    }

    public function getDetails(){
      $user = Auth::user();
      return Response()->json($user,$this->successStatus);
    }

    public function logout(Request $request){
      $user = Auth::user();
      $request->user()->token()->revoke();
      $response = [
        'error'=> true,
        'message'=>'Berhasil Logout'
      ];
      return response()->json($response, 201);
    }
    public function paketcuci(){
      $paket = Paketcuci::all();
      $data = json_decode($paket);
      // return $data;
      // $response = [
      //   'error'=>false,
      //   'namapaket'=> $data->namapaket,
      //   'harga'=> $data->harga
      // ];
      return response()->json($data);
    }
}
