<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Datadepositagen extends Model
{
  protected $fillable = [
      'id','user_id','no_trx','tgl_trx','bank','nominal','transfer','ket','aktif','respon_json','created_at','created_by','updated_at','updated_by','deleted_at','deleted_by'
  ];
}
