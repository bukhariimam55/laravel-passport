<?php
Route::get('logs', '\Rap2hpoutre\LaravelLogViewer\LogViewerController@index')->middleware('admin');
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/users', ['uses' => 'AdminControllerWeb@user', 'middleware' => 'admin']);
Route::get('/condef', ['uses' => 'AdminControllerWeb@condef', 'middleware' => 'admin']);
Route::get('/prosdep{id}', ['uses' => 'AdminControllerWeb@prosdep', 'middleware' => 'admin']);
Route::get('/canceldep{id}', ['uses' => 'AdminControllerWeb@canceldep', 'middleware' => 'admin']);
Route::get('/datadeposit', ['uses' => 'AdminControllerWeb@datadeposit', 'middleware' => 'admin']);
Route::get('/bukusaldo', ['as'=>'bukusaldo','uses' => 'AdminControllerWeb@bukusaldo', 'middleware' => 'admin']);
Route::post('/bukusaldo', ['as'=>'bukusaldo','uses' => 'AdminControllerWeb@bukusaldo', 'middleware' => 'admin']);
Route::get('/datpul', ['uses' => 'AdminControllerWeb@datpul', 'middleware' => 'admin']);
Route::get('/transaksi-agent', ['uses' => 'AdminControllerWeb@transaksiagent', 'middleware' => 'admin']);

Route::get('/dathar', ['uses' => 'AdminControllerWeb@dathar', 'middleware' => 'admin']);
Route::post('/dathar', ['as' => 'admin.dathar','uses' => 'AdminControllerWeb@updateuntung', 'middleware' => 'admin']);
Route::get('/updateharga{id}', ['uses' => 'AdminControllerWeb@updateharga', 'middleware' => 'admin']);

Route::get('/agent', ['uses' => 'AdminControllerWeb@datharagent', 'middleware' => 'admin']);
Route::get('/konfirmasi-deposit-agen', ['uses' => 'AdminControllerWeb@depositagen', 'middleware' => 'admin']);
Route::post('/konfirmasi-deposit-agen', ['uses' => 'AdminControllerWeb@depositagen', 'middleware' => 'admin']);
Route::get('/updateagent{id}', ['uses' => 'AdminControllerWeb@updateagent', 'middleware' => 'admin']);
Route::get('/proses-dep-agen/{id}','AdminControllerWeb@prosesdepagen');
Route::get('/cancel-dep-agen/{id}','AdminControllerWeb@cancelagen');
